// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 16 de octubre del 2023

// Evaluación Parcial N°2 - Unidad I

// Escribe un programa que contabilice cuántas veces aparece un patrón específico en un arreglo de cadenas, en este caso, cuántas veces aparece la palabra "ana" dentro de ejemplos de frutas.

fn contar_patron_en_arreglo(arr: &[&str], patron: &str) -> u32 {

    let mut contador = 0;

    for cadena in arr {
        let mut i = 0;
        let mut j = 0;

        let cadena_chars: Vec<char> = cadena.chars().collect();
        let patron_chars: Vec<char> = patron.chars().collect();

        while i < cadena_chars.len() {

            if cadena_chars[i] == patron_chars[j] {
                i += 1;
                j += 1;

                if j == patron_chars.len() {
                    contador += 1;
                    j = 0;
                }
            } 
            else {
                if j > 0 {
                    j = 0;
                }
                else {
                    i += 1;
                }
            }
        }
    }

    contador
}

fn main() {

    let arreglo_strings = vec!["manzana", "banana", "pera", "uva", "manzana verde", "choclo", "sandia"];
    let patron_busqueda = "ana";
    let cantidad_encontrada = contar_patron_en_arreglo(&arreglo_strings, patron_busqueda);

    println!("Arreglo ingresado: {:?}", arreglo_strings);
    println!("El patrón '{}' aparece {} veces en el arreglo ingresado.", patron_busqueda, cantidad_encontrada);
}
