// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Implementa una función que tome una cadena de texto como entrada y utilice match para validar si contiene solo 
// letras (sin números ni caracteres especiales).

use std::io;

fn contiene_solo_letras(input: &str) -> bool {
    for c in input.chars() {
        match c {
            'a'..='z' | 'A'..='Z' => continue,
            _ => return false,
        }
    }
    true
}

fn main() {
    println!("Ingrese una cadena de texto:");

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Fallo al leer la entrada");

    let texto = input.trim();

    match contiene_solo_letras(texto) {
        true => println!("La cadena contiene solo letras."),
        false => println!("La cadena contiene números o caracteres especiales."),
    }
}
