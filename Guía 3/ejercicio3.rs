// Nombre: Gabriela Antonia Sepúlveda Rojas 
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

//Escriba una función recursiva que calcule el MCD (Máximo Común Divisor) de dos números enteros positivos utilizando
// el algoritmo de Euclides. 

use std::io;

fn mcd(a: u32, b: u32) -> u32 {
    
    if b == 0 {
        return a;
    }
    return mcd(b, a % b);
}

fn main() {
    println!("Ingrese el primer número entero positivo:");
    
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Fallo al leer la entrada");
    let num1: u32 = input.trim().parse().expect("Entrada no válida. Por favor, ingrese un número entero positivo.");
    
    println!("Ingrese el segundo número entero positivo:");
    input.clear();
    io::stdin().read_line(&mut input).expect("Fallo al leer la entrada");
    let num2: u32 = input.trim().parse().expect("Entrada no válida. Por favor, ingrese un número entero positivo.");
    
    let resultado = mcd(num1, num2);
    println!("El Máximo Común Divisor de {} y {} es {}", num1, num2, resultado);
}
