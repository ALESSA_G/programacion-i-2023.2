// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Investigue y escriba una función recursiva que calcule el n-ésimo término de la serie de Tribonacci, donde cada término 
// es la suma de los tres términos anteriores.

use std::io;

fn tribonacci_s(n: u32) -> u32 {
    if n == 0 {
        return 0;
    } else if n == 1 || n == 2 {
        return 1;
    }
    
    return tribonacci_s(n - 1) + tribonacci_s(n - 2) + tribonacci_s(n - 3);
}

fn main() {
    println!("Ingrese el valor de n para calcular el término de la serie de Tribonacci: ");
    
    let mut n_serie = String::new();
    io::stdin().read_line(&mut n_serie).expect("Fallo al leer la entrada");
    
    let n: u32 = n_serie.trim().parse().expect("Entrada no válida. Por favor, ingrese un número entero positivo.");
    
    let resultado = tribonacci_s(n);
    println!("El {}-ésimo término de la serie de Tribonacci es {}", n, resultado);
}