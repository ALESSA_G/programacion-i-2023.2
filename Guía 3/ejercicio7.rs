// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Escriba una función que tome una cadena que represente un día de la semana y utilice match para validar si es un día 
// laborable (lunes a viernes) o un día de fin de semana (sábado o domingo).

use std::io;

fn main() {
    println!("Ingrese un día de la semana:");
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Error al leer la entrada");
    let dia = input.trim();

    match_dia(dia);
}

fn match_dia(dia: &str) {
    match dia.to_lowercase().as_str() {
        "lunes" | "martes" | "miércoles" | "miercoles" | "jueves" | "viernes" => {
            println!("{} es un día laborable.", dia);
        }
        "sábado" | "sabado" | "domingo" => {
            println!("{} es un día de fin de semana.", dia);
        }
        _ => {
            println!("{} no es un día válido.", dia);
        }
    }
}