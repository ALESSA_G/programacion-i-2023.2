// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Crea una función que tome un resultado (Result) de un cálculo y utilice match para manejar tanto el caso exitoso 
// como el caso de error. (OK y Err, Ignorando enlaces a variables).

fn manejo_resultado(resultado: Result<i32, String>) {

    match resultado {
        Ok(valor) => {
            println!("Operación exitosa: {}", valor);
        }
        Err(error) => {
            println!("Error: {}", error);
            // Puedes realizar acciones adicionales aquí en el caso de error.
        }
    }
}

fn main() {
    let exito = Ok(22);
    let error = Err("Algo salió mal, intente nuevamente".to_string());

    manejo_resultado(exito);
    manejo_resultado(error);
}
