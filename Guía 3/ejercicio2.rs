// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Escriba una función recursiva en RUST que imprima una cuenta regresiva desde un numero n hasta 1.

use std::io;

fn cuenta_regresiva(n: u32) {
    if n == 0 {
        println!("La cuenta regresiva ha finalizado.");
    } else {
        println!("{}", n);
        cuenta_regresiva(n - 1);
    }
}

fn main() {
    println!("Ingresa un número para comenzar la cuenta regresiva: ");
    
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Error al leer la entrada");
    
    let n: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            println!("Entrada no válida. Se utilizará 1 como valor predeterminado.");
            1
        }
    };
    cuenta_regresiva(n);
}
