// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Realice la conjetura de ULAM utilizando recursividad (Si es un número par, divida por 2, si es impar divida por tres y sume 1).

use std::io;

fn conjetura_ulam(n: u32) {
    let mut numero = n;

    println!("Secuencia de Ulam:");

    while numero != 1 {
        print!("{} -> ", numero);
        if numero % 2 == 0 {
            numero /= 2;
        } else {
            numero = 3 * numero + 1;
        }
    }

    println!("1");
}

fn main() {
    println!("Ingrese un número para la conjetura de Ulam: ");

    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Fallo al leer la entrada");

    let n: u32 = input
        .trim()
        .parse()
        .expect("Entrada no válida. Por favor, ingrese un número entero positivo.");

    conjetura_ulam(n);
}
