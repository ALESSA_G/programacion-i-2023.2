// Nombre: 20.787.581-3
// RUT: 20.787.581-3
// Fecha: 25 de septiembre del 2023

// Escriba una función recursiva que calcule "a" elevado a la potencia "b", donde a y b son números enteros positivos.

use std::io;

fn main() {

    println!("Ingrese el valor de a:");
    let mut base_a = String::new();
    io::stdin().read_line(&mut base_a).expect("Error al leer la entrada");
    let a: u64 = base_a.trim().parse().expect("Entrada no válida. Debe ser un número entero positivo.");

    println!("Ingrese el valor de b:");
    let mut exp_b = String::new();
    io::stdin().read_line(&mut exp_b).expect("Error al leer la entrada");
    let b: u64 = exp_b.trim().parse().expect("Entrada no válida. Debe ser un número entero positivo.");

    let resultado = potencia(a, b);

    println!("La potencia {}^{} tiene como resultado {}", a, b, resultado);
}

fn potencia(a: u64, b: u64) -> u64 {
    if b == 0 {
        return 1;
    } 
    else {
        return a * potencia(a, b - 1);
    }
}