// Nombre: Gabriela Antonia Sepúlveda Rojas.
// RUT: 20.787.581-3.
// Fecha: 06 de noviembre del 2023.

// Cree un archivo(notas.txt) para almacenar notas de un ramo. Cada línea tiene el nombre del alumno y sus seis notas, separadas por dos puntos (:). Escriba un programa en rust que cree un nuevo archivo llamado reporte.txt, en que cada línea indique si el alumno está aprobado (promedio ≥ 4,0) o reprobado (promedio < 4,0).

use std::fs::File;
use std::io::{self, BufRead, Write};
use std::path::Path;
use std::fs::OpenOptions;

fn create_blank_file(p: &Path) -> File {
    if let Ok(file) = File::create(p) {
        println!("El archivo fue creado con exito.");
        file
    } else {
        panic!("El archivo no pudo crearse correctamente.");
    }
}

fn read_lines<P: AsRef<Path>>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>> {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn open_file_to_append(p: &Path) -> File {
    let file = OpenOptions::new()
        .append(true)
        .open(p)
        .expect("No se puede abrir el archivo correctamente.");
    file
}

fn open_file(p: &Path) -> File {
    if p.exists() {
        File::open(p).expect("El archivo no se puede abrir correctamente.")
    } else {
        create_blank_file(p)
    }
}

fn main() {
    let path = Path::new("reporte.txt");
    let file = open_file(&path);

    let mut file_to_append = open_file_to_append(&path);

    if let Ok(lines) = read_lines("./notas.txt") {
        for line in lines {
            if let Ok(ip) = line {
                let split = ip.split(":");
                let mut suma: f32 = 0.0;
                let mut contador: u32 = 0;

                for s in split {
                    if contador == 0 {
                        println!("{}:", s);
                        file_to_append.write_all(s.as_bytes()).expect("Error al escribir en el archivo.");
                    } else {
                        if let Ok(num) = s.parse::<f32>() {
                            suma += num;
                        } else {
                            panic!("No es un número compatible.");
                        }
                    }

                    let promedio: f32 = suma / 6.0;

                    if contador == 6 {
                        if promedio < 4.0 {
                            println!("Reprobó con una nota igual a {:.1}", promedio);
                            file_to_append.write_all(b": Reprobado\n").expect("Error al escribir en el archivo");
                        } else {
                            println!("Aprobó con una nota igual a {:.1}", promedio);
                            file_to_append.write_all(b": Aprobado\n").expect("Error al escribir en el archivo");
                        }
                    }

                    contador += 1;
                }
            }
        }
    }
    file_to_append.write_all(b"\n").expect("Error al escribir en el archivo");
}
