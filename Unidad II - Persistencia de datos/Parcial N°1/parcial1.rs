// Integrantes: Ariel Celis - Gabriela Sepúlveda.
// Fecha: 22 de noviembre del 2023.
// Base de datos: Pokemon Data Set.

// Parcial N°1 - Unidad N°2

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

//Función: Define un Pokémon por nombre, tipo 1, tipo 2, defensa, velocidad y generación.
struct Pokemon {
    name: String,
    type1: String,
    type2: String,
    defense: i32,
    speed: i32,
    generation: i32,
}

// Función: Toma una línea de datos en forma de cadena y la convierte en un Pokemon si tiene al menos 12 campos separados por ",".
fn parse_pokemon_data(line: &str) -> Option<Pokemon> {
    let fields: Vec<&str> = line.split(',').collect();
    if fields.len() >= 12 {
        Some(Pokemon {
            name: fields[1].to_string(),
            type1: fields[2].to_string(),
            type2: fields[3].to_string(),
            defense: fields[7].parse().unwrap_or(0),
            speed: fields[10].parse().unwrap_or(0),
            generation: fields[11].parse().unwrap_or(0),
        })
    } 
    else {
        None
    }
}

// Función: Abre el archivo, Lee cada línea y convierte los datos a un Pokemon. Cuenta la cantidad de cada tipo de Pokémon e Identifica el tipo más común.
fn count_pokemon_types(filename: &str) {

    let file = open_file(&filename);
    let reader = io::BufReader::new(file);

    let mut type_counts = [[0; 18]; 2]; 

    for line in reader.lines() {
        if let Ok(line) = line {
            if let Some(pokemon) = parse_pokemon_data(&line) {
                increment_type_count(&mut type_counts, &pokemon.type1);
                increment_type_count(&mut type_counts, &pokemon.type2);
            }
        }
    }

    let most_common_type = most_common(&type_counts);

    println!("El tipo de Pokémon más común es: {}.", most_common_type);
}

// Función: Incrementa el recuento para un tipo de Pokémon específico en la matriz type_counts.
fn increment_type_count(type_counts: &mut [[i32; 18]; 2], type_name: &str) {

    match type_name {

        "Bug" => type_counts[0][0] += 1,
        "Dark" => type_counts[0][1] += 1,
        "Dragon" => type_counts[0][2] += 1,
        "Electric" => type_counts[0][3] += 1,
        "Fairy" => type_counts[0][4] += 1,
        "Fighting" => type_counts[0][5] += 1,
        "Fire" => type_counts[0][6] += 1,
        "Flying" => type_counts[0][7] += 1,
        "Ghost" => type_counts[0][8] += 1,
        "Grass" => type_counts[0][9] += 1,
        "Ground" => type_counts[0][10] += 1,
        "Ice" => type_counts[0][11] += 1,
        "Normal" => type_counts[0][12] += 1,
        "Poison" => type_counts[0][13] += 1,
        "Psychic" => type_counts[0][14] += 1,
        "Rock" => type_counts[0][15] += 1,
        "Steel" => type_counts[0][16] += 1,
        "Water" => type_counts[0][17] += 1,
        _ => {}

    }
}

// Función: Encuentra el tipo de Pokémon más común en función del recuento en la matriz type_counts.
fn most_common(type_counts: &[[i32; 18]; 2]) -> &str {

    let mut max_count = 0;
    let mut most_common_type = "";

    for i in 0..18 {
        for j in 0..2 {
            if type_counts[j][i] > max_count {
                max_count = type_counts[j][i];
                most_common_type = match j {
                    0 => match i {
                        0 => "Bug",
                        1 => "Dark",
                        2 => "Dragon",
                        3 => "Electric",
                        4 => "Fairy",
                        5 => "Fighting",
                        6 => "Fire",
                        7 => "Flying",
                        8 => "Ghost",
                        9 => "Grass",
                        10 => "Ground",
                        11 => "Ice",
                        12 => "Normal",
                        13 => "Poison",
                        14 => "Psychic",
                        15 => "Rock",
                        16 => "Steel",
                        17 => "Water",
                        _ => ""
                    },
                    1 => "",
                    _ => ""
                };
            }
        }
    }

    most_common_type

}

// Función: Encuentra y muestra el Pokémon más rápido leyendo el archivo y comparando los valores.
fn fastest_pokemon(filename: &str) {

    let file = open_file(&filename);
    let reader = io::BufReader::new(file);

    let mut fastest_speed = 0;
    let mut fastest_pokemon = String::new(); 

    for line in reader.lines() {
        if let Ok(line) = line {
            if let Some(pokemon) = parse_pokemon_data(&line) {
                if pokemon.speed > fastest_speed {
                    fastest_speed = pokemon.speed;
                    fastest_pokemon = pokemon.name.clone(); 
                }
            }
        }
    }

    println!("El Pokémon más rápido es: {}", fastest_pokemon);

}

// Función: Encuentra y muestra el Pokémon con la defensa más alta leyendo el archivo y comparando los valores.
fn highest_defense_pokemon(filename: &str) {

    let file = open_file(&filename);
    let reader = io::BufReader::new(file);

    let mut highest_defense = 0;
    let mut highest_defense_pokemon = String::new();

    for line in reader.lines() {
        if let Ok(line) = line {
            if let Some(pokemon) = parse_pokemon_data(&line) {
                if pokemon.defense > highest_defense {
                    highest_defense = pokemon.defense;
                    highest_defense_pokemon = pokemon.name.clone(); 
                }
            }
        }
    }

    println!("El Pokémon con mayor defensa es: {}.", highest_defense_pokemon);

}

// Función: Cuenta y muestra la cantidad de Pokémon de la generación 1 leyendo el archivo.
fn generation_1_count(filename: &str) {

    let file = open_file(&filename);
    let reader = io::BufReader::new(file);

    let mut gen_1_count = 0;

    for line in reader.lines() {
        if let Ok(line) = line {
            if let Some(pokemon) = parse_pokemon_data(&line) {
                if pokemon.generation == 1 {
                    gen_1_count += 1;
                }
            }
        }
    }

    println!("Cantidad de Pokémon de la generación 1: {} Pokémon's.", gen_1_count);
}

// Función: Abre un archivo y maneja posibles errores.
fn open_file(filename: &str) -> File {
    
    let file_path = Path::new(filename);
    match File::open(&file_path) {
        Err(why) => panic!("{}", why),
        Ok(file) => file,
    }
}

fn main() {

    let filename = "Pokemon.csv";
    //CÓDIGO ASCII - POKEMON LOGO Y BULBASAUR//
    let pokemon_title = r#" 
                                  ,'\
    _.----.        ____         ,'  _\   ___    ___     ____
_,-'       `.     |    |  /`.   \,-'    |   \  /   |   |    \  |`.
\      __    \    '-.  | /   `.  ___    |    \/    |   '-.   \ |  |
 \.    \ \   |  __  |  |/    ,','_  `.  |          | __  |    \|  |
   \    \/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |
    \     ,-'/  /   \    ,'   | \/ / ,`.|         /  /   \  |     |
     \    \ |   \_/  |   `-.  \    `'  /|  |    ||   \_/  | |\    |
      \    \ \      /       `-.`.___,-' |  |\  /| \      /  | |   |
       \    \ `.__,'|  |`-._    `|      |__| \/ |  `.__,'|  | |   |
        \_.-'       |__|    `-._ |              '-.|     '-.| |   |
                                `'                            '-._
    
                                /
                                _,.------....___,.' ',.-.
                             ,-'          _,.--"        |
                           ,'         _.-'              .
                          /   ,     ,'                   `
                         .   /     /                     ``.
                         |  |     .                       \.\
               ____      |___._.  |       __               \ `.
             .'    `---""       ``"-.--"'`  \               .  \
            .  ,            __               `              |   .
            `,'         ,-"'  .               \             |    L
           ,'          '    _.'                -._          /    |
          ,`-.    ,".   `--'                      >.      ,'     |
         . .'\'   `-'       __    ,  ,-.         /  `.__.-      ,'
         ||:, .           ,'  ;  /  / \ `        `.    .      .'/
         j|:D  \          `--'  ' ,'_  . .         `.__, \   , /
        / L:_  |                 .  "' :_;                `.'.'
        .    ""'                  """""'                    V
         `.                                 .    `.   _,..  `
           `,_   .    .                _,-'/    .. `,'   __  `
            ) \`._        ___....----"'  ,'   .'  \ |   '  \  .
           /   `. "`-.--"'         _,' ,'     `---' |    `./  |
          .   _  `""'--.._____..--"   ,             '         |
          | ." `. `-.                /-.           /          ,
          | `._.'    `,_            ;  /         ,'          .
         .'          /| `-.        . ,'         ,           ,
         '-.__ __ _,','    '`-..___;-...__   ,.'\ ____.___.'
         `"^--'..'   '-`-^-'"--    `-^-'`.''"""""`.,^.`.--' 
    "#;
    println!("{}", pokemon_title);

    count_pokemon_types(&filename);
    fastest_pokemon(&filename);
    highest_defense_pokemon(&filename);
    generation_1_count(&filename);

}
