// Integrantes: Ariel Celis - Gabriela Sepúlveda.
// Fecha: 29 de noviembre del 2023.
// Base de datos: Pokemon Data Set.

// Parcial N°2 - Unidad N°2

use std::fs::{File, OpenOptions};
use std::io::{self, BufRead, BufReader, Write};

// Definición de la estructura Pokemon
struct Pokemon {
    num: i32,
    name: String,
    type1: String,
    type2: String,
    total: i32,
    hp: i32,
    attack: i32,
    defense: i32,
    sp_attack: i32,
    sp_defense: i32,
    speed: i32,
    generation: i32,
    legendary: String,
}

impl Pokemon {
    // Método para agregar un Pokémon al archivo.
    fn agregar_pokemon(&self, filename: &str) {
        // Abrir el archivo en modo escritura y apertura al final.
        let mut file = match OpenOptions::new().write(true).append(true).create(true).open(filename) {
            Ok(f) => f,
            Err(e) => {
                panic!("No se pudo abrir ni crear el archivo: {}", e);
            }
        };

        // Crear una línea con los datos del Pokémon.
        let pokemon_line = format!(
            "{},{},{},{},{},{},{},{},{},{},{},{},{}\n",
            self.num,
            self.name,
            self.type1,
            self.type2,
            self.total,
            self.hp,
            self.attack,
            self.defense,
            self.sp_attack,
            self.sp_defense,
            self.speed,
            self.generation,
            self.legendary,
        );

        // Escribir la línea en el archivo.
        match file.write_all(pokemon_line.as_bytes()) {
            Ok(_) => println!("Datos del Pokémon agregados al archivo."),
            Err(e) => println!("Error al escribir en el archivo: {}", e),
        }
    }
}

// Función para mostrar todos los Pokémon en el archivo.
fn mostrar_pokemones(filename: &str) {

    // Abrir el archivo.
    if let Ok(file) = File::open(filename) {

        let reader = io::BufReader::new(file);
        // Imprimir encabezados de los datos impresos.
        println!("ID | Número | Nombre | Tipo 1 | Tipo 2 | Total | HP | Ataque | Defensa | Velocidad de Ataque | Velocidad de Defensa | Velocidad | Generación | Legendario |");

        let mut count = 1;
        // Iterar sobre las líneas del archivo e imprimir cada Pokémon
        for line in reader.lines() {
            if let Ok(pokemon_data) = line {
                println!("{} | {}", count, pokemon_data);
                count += 1;
            }
        }
    } else {
        println!("No se pudo abrir el archivo.");
    }
}

// Función para buscar un Pokémon por un término específico, solicitado por el usuario.
fn buscar_pokemon(filename: &str, search_term: &str) {

    // Abrir el archivo.
    if let Ok(file) = File::open(filename) {
        let reader = io::BufReader::new(file);

        let mut found = false;

        println!("Resultados de la búsqueda:");

        // Iterar sobre las líneas del archivo y buscar el término solicitado.
        for line in reader.lines() {
            if let Ok(pokemon_data) = line {
                if pokemon_data.contains(search_term) {
                    println!("{}", pokemon_data);
                    found = true;
                }
            }
        }

        if !found {
            println!("No se encontraron resultados para '{}'.", search_term);
        }
    } else {
        println!("No se pudo abrir el archivo.");
    }
}

// Función para eliminar un Pokémon por número de fila.
fn eliminar_pokemon(filename: &str, row_number: usize) {

    // Abrir el archivo.
    let file = match File::open(filename) {
        Ok(file) => file,
        Err(_) => {
            println!("No se pudo abrir el archivo.");
            return;
        }
    };

    let reader = BufReader::new(file);
    let mut lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    // Verificar si el número de fila es correcto.
    if row_number > lines.len() {
        println!("Número de fila no válido.");
        return;
    }

    // Eliminar la línea correspondiente al Pokémon solicitado.
    lines.remove(row_number - 1);

    // Crear un archivo temporal
    let temp_filename = "temp.txt";
    let mut temp_file = match File::create(temp_filename) {
        Ok(file) => file,
        Err(_) => {
            println!("No se pudo crear el archivo temporal.");
            return;
        }
    };

    // Escribir las líneas restantes en el archivo temporal.
    for line in &lines {
        if let Err(_) = writeln!(&mut temp_file, "{}", line) {
            println!("Error al escribir en el archivo temporal.");
            return;
        }
    }

    // Renombrar el archivo temporal al original.
    if let Err(_) = std::fs::rename(temp_filename, filename) {
        println!("Error al renombrar el archivo temporal.");
        return;
    }

    println!("Fila {} eliminada con éxito.", row_number);
}

fn editar_pokemon(filename: &str, row_number: usize) {

    // Abrir el archivo en modo lectura y escritura.
    let file = match OpenOptions::new().read(true).write(true).open(filename) {
        Ok(file) => file,
        Err(_) => {
            println!("No se pudo abrir el archivo.");
            return;
        }
    };

    let reader = BufReader::new(&file);
    let mut lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    // Verificar si el número de fila es correcto.
    if row_number > lines.len() {
        println!("Número de fila no válido.");
        return;
    }

    println!("Editar Pokémon en la fila {}:", row_number);
    println!("Ingrese los nuevos datos siguiendo el formato del archivo:");
    println!("Número,Nombre,Tipo 1,Tipo 2,Total,HP,Ataque,Defensa,Velocidad de Ataque,Velocidad de Defensa,Velocidad,Generación,Legendario");

    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    // Actualizar la línea correspondiente al Pokémon con los nuevos datos.
    lines[row_number - 1] = input.trim().to_string();
    // Crear un archivo temporal para escribir los datos actualizados del Pokémon.
    let mut file = match File::create(filename) {
        Ok(file) => file,
        Err(_) => {
            println!("No se pudo crear el archivo temporal.");
            return;
        }
    };
    // Escribir las líneas actualizadas en el archivo original.
    for line in &lines {
        if let Err(_) = writeln!(&mut file, "{}", line) {
            println!("Error al escribir en el archivo temporal.");
            return;
        }
    }

    println!("Fila {} editada con éxito.", row_number);
}

// Función para obtener los datos de un nuevo Pokémon desde el usuario.
fn obtener_datos_pokemon() -> Pokemon {

    // Solicitar al usuario los datos para un nuevo Pokémon.
    println!("Ingrese los datos del nuevo Pokémon:");
    let mut input = String::new();

    println!("- Número: ");
    io::stdin().read_line(&mut input).unwrap();
    let num: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Nombre: ");
    io::stdin().read_line(&mut input).unwrap();
    let name = input.trim().to_string();
    input.clear();

    println!("- Tipo 1: ");
    io::stdin().read_line(&mut input).unwrap();
    let type1 = input.trim().to_string();
    input.clear();

    println!("- Tipo 2: ");
    io::stdin().read_line(&mut input).unwrap();
    let type2 = input.trim().to_string();
    input.clear();

    println!("- Total: ");
    io::stdin().read_line(&mut input).unwrap();
    let total: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- HP: ");
    io::stdin().read_line(&mut input).unwrap();
    let hp: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Ataque: ");
    io::stdin().read_line(&mut input).unwrap();
    let attack: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Defensa: ");
    io::stdin().read_line(&mut input).unwrap();
    let defense: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Velocidad de Ataque: ");
    io::stdin().read_line(&mut input).unwrap();
    let sp_attack: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Velocidad de Defensa: ");
    io::stdin().read_line(&mut input).unwrap();
    let sp_defense: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Velocidad: ");
    io::stdin().read_line(&mut input).unwrap();
    let speed: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Generación: ");
    io::stdin().read_line(&mut input).unwrap();
    let generation: i32 = input.trim().parse().unwrap();
    input.clear();

    println!("- Legendario (True o False): ");
    io::stdin().read_line(&mut input).unwrap();
    let legendary = input.trim().to_string();
    input.clear();

    let pokemon = Pokemon {
        num,
        name,
        type1,
        type2,
        total,
        hp,
        attack,
        defense,
        sp_attack,
        sp_defense,
        speed,
        generation,
        legendary,
    };

    pokemon

}

fn main() {
    loop {
        // Mostrar el menú de opciones.
        println!("MENU DE CONTENIDO"); 
        println!("1. Mostrar lista de Pokémones");
        println!("2. Agregar nuevo Pokémon");
        println!("3. Eliminar Pokémon");
        println!("4. Buscar Pokémon");
        println!("5. Editar Pokémon");
        println!("6. Salir");

        // Leer la opción del usuario.
        let mut choice = String::new();
        io::stdin().read_line(&mut choice).unwrap();
        let choice: u32 = match choice.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        // Realizar acciones según la opción seleccionada por el usuario.
        match choice {
            1 => {
                mostrar_pokemones("Pokemon.csv");
            }
        
            2 => {
                let new_pokemon = obtener_datos_pokemon();
                new_pokemon.agregar_pokemon("Pokemon.csv");
                println!("Nuevo Pokémon agregado al archivo.");
            }
        
            3 => {
                println!("Ingrese el número de fila del Pokémon a eliminar:");
                let mut input = String::new();
                io::stdin().read_line(&mut input).expect("Error al leer la entrada");
        
                let row_number: usize = match input.trim().parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Número de fila inválido.");
                        continue;
                    }
                };
        
                eliminar_pokemon("Pokemon.csv", row_number);
                println!("Eliminación completada.");
            }
        
            4 => {
                println!("Ingrese el término de búsqueda:");
                let mut input = String::new();
                io::stdin().read_line(&mut input).expect("Error al leer la entrada");
        
                let search_term = input.trim();
                
                buscar_pokemon("Pokemon.csv", search_term);
                println!("Búsqueda completada.");
            }
        
            5 => {
                println!("Ingrese el número de fila del Pokémon a editar:");
                let mut input = String::new();
                io::stdin().read_line(&mut input).expect("Error al leer la entrada");
        
                let row_number: usize = match input.trim().parse() {
                    Ok(num) => num,
                    Err(_) => {
                        println!("Número de fila inválido.");
                        continue;
                    }
                };
        
                editar_pokemon("Pokemon.csv", row_number);
                println!("Edición completada.");
            }
        
            6 => {
                println!("Saliendo del programa.");
                break;
            }
            _ => println!("Opción no válida, por favor elija 1, 2, 3, 4, 5 o 6."),
        }
        
    }
}