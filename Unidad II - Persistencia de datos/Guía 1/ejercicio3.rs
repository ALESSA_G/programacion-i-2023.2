// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 23 de octubre del 2023

// Registro de Películas: Crea una estructura Película con campos como título, director y año de lanzamiento. 
// Luego, implementa una función que tome una lista de películas y busque las películas lanzadas en un año específico.
// Esto debe permitir al usuario ingresar detalles de varias películas y muestra las que cumplen con el criterio de búsqueda.

use std::io;

struct Pelicula {
    titulo: String,
    director: String,
    anio_pelicula: i32,
}

fn peliculas_por_anio(peliculas: &[Pelicula], anio: i32) -> Vec<&Pelicula> {
    let mut peliculas_encontradas: Vec<&Pelicula> = Vec::new();
    
    for pelicula in peliculas {
        if pelicula.anio_pelicula == anio {
            peliculas_encontradas.push(pelicula);
        }
    }
    peliculas_encontradas
}

fn main() {
    let mut lista_peliculas: Vec<Pelicula> = Vec::new();

    println!("Ingrese los datos de las películas (o escriba '0' para terminar): ");

    loop {
        println!("Ingrese el título de la película: ");
        let mut titulo = String::new();
        io::stdin().read_line(&mut titulo).expect("Fallo al leer la entrada");
        let titulo = titulo.trim();

        if titulo.to_lowercase() == "0" {
            break;
        }

        println!("Ingrese el director de la película: ");
        let mut director = String::new();
        io::stdin().read_line(&mut director).expect("Fallo al leer la entrada");
        let director = director.trim();

        println!("Ingrese el año de lanzamiento de la película: ");
        let mut anio_input = String::new();
        io::stdin().read_line(&mut anio_input).expect("Fallo al leer la entrada");

        let anio_pelicula: i32 = match anio_input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Año de lanzamiento no válido. Se establecerá en 0.");
                0
            }
        };

        let nueva_pelicula = Pelicula {
            titulo: String::from(titulo),
            director: String::from(director),
            anio_pelicula,
        };
        lista_peliculas.push(nueva_pelicula);
    }

    println!("Ingrese el año para buscar las películas lanzadas en ese año: ");
    let mut anio_busqueda = String::new();
    io::stdin().read_line(&mut anio_busqueda).expect("Fallo al leer la entrada");
    let anio_busqueda: i32 = anio_busqueda.trim().parse().expect("Año de búsqueda no válido");

    let peliculas_encontradas = peliculas_por_anio(&lista_peliculas, anio_busqueda);

    println!("Películas lanzadas en el año {}: ", anio_busqueda);

    for pelicula in peliculas_encontradas {
        println!("- Título: {} - Director: {} - Año de Lanzamiento: {}", pelicula.titulo, pelicula.director, pelicula.anio_pelicula);
    }
}