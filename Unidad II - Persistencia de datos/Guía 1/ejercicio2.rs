// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 23 de octubre del 2023

// Registro de estudiantes: Define una estructura Estudiante con campos como nombre, matricula y calificaciones (un arreglo de calificaciones). 
// Escribe una función que calcule el promedio de calificaciones de un estudiante y determine si está aprobado o no (promedio igual o mayor a 40). 
// Luego, permite al usuario ingresar detalles de varios estudiantes y muestra sus promedios y estado de aprobación.

use std::io;

struct Estudiante {
    nombre: String,
    matricula: String,
    calificaciones: Vec<f32>,
}

impl Estudiante {
    
    fn calc_promedio(&self) -> f32 {
        let sum: f32 = self.calificaciones.iter().sum();
        sum / self.calificaciones.len() as f32
    }

    fn aprobo(&self) -> bool {
        let promedio = self.calc_promedio();
        promedio >= 4.0
    }
}

fn main() {
    let mut lista_estudiantes: Vec<Estudiante> = Vec::new();

    println!("Ingrese los datos de los estudiantes (o escriba '0' para terminar de ingresar): ");

    loop {
        println!("Ingrese el nombre del estudiante: ");
        let mut nombre = String::new();
        io::stdin().read_line(&mut nombre).expect("Fallo al leer la entrada");
        let nombre = nombre.trim();

        if nombre.to_lowercase() == "0" {
            break;
        }

        println!("Ingrese la matrícula del estudiante: ");
        let mut matricula = String::new();
        io::stdin().read_line(&mut matricula).expect("Fallo al leer la entrada");
        let matricula = matricula.trim();

        println!("Ingrese las calificaciones del estudiante (separadas por espacio y utilizando puntuación): ");
        let mut calif_input = String::new();
        io::stdin().read_line(&mut calif_input).expect("Fallo al leer la entrada");
        let calificaciones: Vec<f32> = calif_input.split_whitespace().filter_map(|s| s.parse().ok()).collect();

        let nuevo_estudiante = Estudiante {
            nombre: String::from(nombre),
            matricula: String::from(matricula),
            calificaciones,
        };

        lista_estudiantes.push(nuevo_estudiante);

    }

    println!("*Promedio y estado de los estudiantes*");

    for estudiante in &lista_estudiantes {
        let promedio = estudiante.calc_promedio();
        let aprobado = estudiante.aprobo();

        println!("- Estudiante: {} - Matrícula: {} - Promedio: {:.1} - Estado: {}", estudiante.nombre, estudiante.matricula, promedio, if aprobado { "Aprobado" } else { "Reprobado" });
    }
}
