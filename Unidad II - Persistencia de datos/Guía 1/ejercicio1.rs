// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 23 de octubre del 2023

// Registro de Libros: Crea una estructura llamada Libro que represente un libro con campos como título, autor y año de publicación. 
// Luego, escribe un programa que permita al usuario ingresar detalles de varios libros y los almacene en un arreglo. 
// Después, muestra la lista de libros ingresados. Crea valores por defecto para definir el arreglo de libros.

use std::io;

struct Libro {
    titulo: String,
    autor: String,
    ano_publicacion: i32,
}

fn main() {

    println!("*Biblioteca digital*");
  
    let mut biblioteca: Vec<Libro> = Vec::new();

    let mut contador = 1;
    loop {
        println!("Ingrese el título del libro (o escriba '0' para terminar): ");
        let mut titulo = String::new();
        io::stdin().read_line(&mut titulo).expect("Fallo al leer la entrada");
        let titulo = titulo.trim();

        if titulo.to_lowercase() == "0" {
            break;
        }

        println!("Ingrese el autor del libro: ");
        let mut autor = String::new();
        io::stdin().read_line(&mut autor).expect("Fallo al leer la entrada");
        let autor = autor.trim();

        println!("Ingrese el año de publicación del libro: ");
        let mut ano_input = String::new();
        io::stdin().read_line(&mut ano_input).expect("Fallo al leer la entrada");

        let ano_publicacion: i32 = match ano_input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Año de publicación no válido. Se establecerá en 0.");
                0
            }
        };

        let nuevo_libro = Libro {
            titulo: String::from(titulo),
            autor: String::from(autor),
            ano_publicacion,
        };
        biblioteca.push(nuevo_libro);

        println!("Libro {} agregado.\n", contador);
        contador += 1;
    }

    println!("*Lista de libros ingresados*");
    for (i, libro) in biblioteca.iter().enumerate() {
        println!("Libro {}: {} de {} (Año: {})", i + 1, libro.titulo, libro.autor, libro.ano_publicacion);
    }
}