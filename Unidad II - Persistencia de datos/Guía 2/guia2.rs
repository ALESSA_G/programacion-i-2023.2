// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 13 de noviembre del 2023

// Responder seis preguntas relacionadas a una lista de música, utilizando el archivo Spotify_final_dataset.csv.

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

// Función para encontrar al artista con más canciones en la lista.
fn artista_con_mas_canciones(filename: &str) {
    let file = abrir_archivo(&filename);
    let reader = io::BufReader::new(file);
    let mut conteo_artistas: std::collections::HashMap<String, u32> = std::collections::HashMap::new();

    // Contar las canciones por artista.
    for line in reader.lines() {
        if let Ok(linea) = line {
            let campos: Vec<&str> = linea.split(';').collect();
            if campos.len() > 1 {
                let artista = campos[1].to_string();
                let conteo = conteo_artistas.entry(artista).or_insert(0);
                *conteo += 1;
            }
        }
    }

    if let Some((artista_max, total_canciones_max)) = conteo_artistas.iter().max_by_key(|&(_, conteo)| conteo) {

        println!("El artista con más canciones en el ranking es: '{}' con {} canciones.", artista_max, total_canciones_max);
    } 
    else {
        println!("No se encontraron artistas en el archivo.");
    }
}

// Función para encontrar el valor máximo relacionado a las preguntas.
fn encontrar_max_valor(archivo: File, campo: usize, pregunta: &str) {

    let posicion_artista = 1;
    let posicion_cancion = 2;
    let posicion_a_encontrar = campo;
    let reader = io::BufReader::new(archivo);
    let mut maximo_streams = 0;
    let mut nombre_cancion_max = String::new();
    let mut nombre_artista_max = String::new();

    for linea in reader.lines() {
        if let Ok(linea) = linea {
            let campos: Vec<&str> = linea.split(';').collect();

            if campos.len() <= 9 {
                let nombre_artista = campos[posicion_artista];
                let nombre_cancion = campos[posicion_cancion];
                let total_streams = campos[posicion_a_encontrar].trim().parse::<i32>();

                if let Ok(streams) = total_streams {
                    if streams > maximo_streams {
                        maximo_streams = streams;
                        nombre_cancion_max = nombre_cancion.to_string();
                        nombre_artista_max = nombre_artista.to_string();
                    }
                    else {
                        println!("Error en la lectura del campo");
                    }
                }
            }
        }
    }

    println!("{} {} de {}", pregunta, nombre_cancion_max, nombre_artista_max);
}

// Función para abrir el archivo.
fn abrir_archivo(nombre_archivo: &str) -> File {
    let ruta_archivo = Path::new(nombre_archivo);
    match File::open(&ruta_archivo) {
        Err(why) => panic!("{}", why),
        Ok(archivo) => archivo,
    }
}

fn main() {
    let nombre_archivo = "Spotify_final_dataset.csv";

    //Preguntas y sus respuestas.
    encontrar_max_valor(abrir_archivo(&nombre_archivo), 6, "- La canción que más veces alcanzó la posición máxima es:");
    encontrar_max_valor(abrir_archivo(&nombre_archivo), 8, "- La canción que más reproducciones ha tenido es:");
    encontrar_max_valor(abrir_archivo(&nombre_archivo), 7, "- La canción que más reproducciones ha tenido cuando ha estado popular es:");
    encontrar_max_valor(abrir_archivo(&nombre_archivo), 4, "- La canción que ha tenido el mayor número de veces dentro del top 10 es:");
    encontrar_max_valor(abrir_archivo(&nombre_archivo), 3, "- La canción más antigua es:");

    artista_con_mas_canciones(&nombre_archivo);
}