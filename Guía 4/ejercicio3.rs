// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Escribe una función que tome un arreglo y un valor como entrada y devuelva true si el valor está presente en el arreglo y false 
// en caso contrario.

fn valor_presente(arr: &[i32], valor: i32) -> bool {
    for &elemento in arr {
        if elemento == valor {
            return true;
        }
    }
    false
}

fn main() {
    let arreglo = [22, 12, 35, 81, 5];
    let valor = 4;
    println!("Arreglo: {:?}", arreglo);

    if valor_presente(&arreglo, valor) {
        println!("El valor {} está presente en el arreglo.", valor);
    } else {
        println!("El valor {} no está presente en el arreglo.", valor);
    }
}