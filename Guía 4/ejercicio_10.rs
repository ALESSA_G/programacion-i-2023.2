// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Escribe una función que tome dos matrices como entrada y devuelva una nueva matriz que sea el resultado de sumar las dos matrices. 
// Asegúrate de verificar que las matrices tengan las mismas dimensiones antes de realizar la suma.

fn suma_matrices(matriz_a: &[Vec<i32>], matriz_b: &[Vec<i32>]) -> Option<Vec<Vec<i32>>> {
    let filas_a = matriz_a.len();
    let col_a = if filas_a > 0 { matriz_a[0].len() } else { 0 };
    let filas_b = matriz_b.len();
    let col_b = if filas_b > 0 { matriz_b[0].len() } else { 0 };

    if filas_a != filas_b || col_a != col_b {
        return None; 
    }

    let mut resultado = vec![vec![0; col_a]; filas_a];

    for i in 0..filas_a {
        for j in 0..col_a {
            resultado[i][j] = matriz_a[i][j] + matriz_b[i][j];
        }
    }

    Some(resultado)
}

fn main() {
    let matriz_a = vec![
        vec![0, 2],
        vec![3, 1],
    ];

    let matriz_b = vec![
        vec![5, 6],
        vec![3, 8],
    ];

    match suma_matrices(&matriz_a, &matriz_b) {
        Some(resultado) => {
            println!("Resultado de la suma: ");
            for fila in resultado.iter() {
                println!("{:?}", fila);
            }
        }
        None => {
            println!("Las matrices no tienen las mismas dimensiones.");
        }
    }
}
