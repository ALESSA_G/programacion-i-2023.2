// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea una función en rust que encuentre el valor mínimo y el valor máximo en un arreglo de números enteros ingresado por el usuario.

use std::io;

fn encontrar_min_max(arreglo: &Vec<i32>) -> (i32, i32) {
    if arreglo.is_empty() {
        panic!("El arreglo está vacío");
    }

    let mut minimo = arreglo[0];
    let mut maximo = arreglo[0];

    for &numero in arreglo {
        if numero < minimo {
            minimo = numero;
        }
        if numero > maximo {
            maximo = numero;
        }
    }

    (minimo, maximo)
}

fn main() {
    println!("Ingrese números separados por espacios: ");

    let mut entrada = String::new();
    io::stdin().read_line(&mut entrada).expect("Error al leer la entrada");

    let numeros: Vec<i32> = entrada.split_whitespace().map(|s| s.parse().expect("No se pudo convertir a número")).collect();

    let (minimo, maximo) = encontrar_min_max(&numeros);

    println!("Valor mínimo del arreglo: {}", minimo);
    println!("Valor máximo del arreglo: {}", maximo);
}