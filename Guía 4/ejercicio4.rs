// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea una función que cuente cuántos números pares hay en un arreglo de enteros ingresados por el usuario.

use std::io;

fn main() {
    println!("Indique la cantidad de valores que ingresará: ");
    let mut cantidad = String::new();
    io::stdin().read_line(&mut cantidad).expect("Error al leer la entrada");

    let cantidad: usize = cantidad.trim().parse().expect("No se pudo convertir a número entero");

    let mut numeros = Vec::new();

    for i in 0..cantidad {

        println!("Ingresa el número {}: ", i + 1);
        let mut entrada = String::new();
        io::stdin().read_line(&mut entrada).expect("Error al leer la entrada");

        let numero: i32 = entrada.trim().parse().expect("No se pudo convertir a número entero");

        numeros.push(numero);
    }

    let cant_pares = numeros.iter().filter(|&x| x % 2 == 0).count();
    println!("Existe(n) {} número(s) par(es) en el arreglo", cant_pares);
}