// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Escribe una función que tome dos arreglos de números y devuelva un nuevo arreglo que sea el resultado de multiplicar elemento 
// por elemento los dos arreglos.

fn multiplicar_arreglos(arr1: &[i32], arr2: &[i32]) -> Vec<i32> {
    if arr1.len() != arr2.len() {
        panic!("Los arreglos deben tener la misma longitud");
    }

    let mut resultado = vec![0; arr1.len()];

    for i in 0..arr1.len() {
        resultado[i] = arr1[i] * arr2[i];
    }

    resultado
}

fn main() {
    let arreglo1 = vec![1, 3, 5, 7, 9];
    let arreglo2 = vec![2, 4, 6, 8, 10];

    let resultado = multiplicar_arreglos(&arreglo1, &arreglo2);

    println!("Arreglo 1: {:?}", arreglo1);
    println!("Arreglo 2: {:?}", arreglo2);
    println!("Multiplicación de ambos arreglos: {:?}", resultado);
}
