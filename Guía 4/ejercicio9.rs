// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea un nuevo arreglo de numeros ordenados a partir de otro previamente definido.

fn main() {
    let mut arreglo = vec![5, 9, 2, 8, 1, 3, 7, 4, 6];

    println!("Arreglo original: {:?}", numeros);

    numeros.sort();

    println!("Arreglo ordenado: {:?}", numeros);
}