// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea una función que tome dos matrices como entrada y devuelva una nueva matriz que sea el resultado de multiplicar las dos matrices.
// Asegúrate de que las matrices sean compatibles para la multiplicación (el número de columnas de la primera matriz debe ser igual al 
// número de filas de la segunda matriz).

fn multiplicar_matrices(matriz_1: &[Vec<i32>], matriz_2: &[Vec<i32>]) -> Option<Vec<Vec<i32>>> {
    let fila_a = matriz_1.len();
    let col_a = if fila_a > 0 { matriz_1[0].len() } else { 0 };
    let fila_b = matriz_2.len();
    let col_b = if fila_b > 0 { matriz_2[0].len() } else { 0 };

    if col_a != fila_b {
        return None; 
    }

    let mut resultado = vec![vec![0; col_b]; fila_a];

    for i in 0..fila_a {
        for j in 0..col_b {
            for k in 0..col_a {
                resultado[i][j] += matriz_1[i][k] * matriz_2[k][j];
            }
        }
    }

    Some(resultado)
}

fn main() {
    let matriz_1 = vec![
        vec![1, 2],
        vec![3, 4],
    ];

    let matriz_2 = vec![
        vec![5, 6],
        vec![7, 8],
    ];

    match multiplicar_matrices(&matriz_1, &matriz_2) {
        Some(resultado) => {
            println!("Matriz 1: ");
            imprimir_matriz(&matriz_1);
            
            println!("Matriz 2: ");
            imprimir_matriz(&matriz_2);
            
            println!("Resultado de la multiplicación: ");
            imprimir_matriz(&resultado);
        }
        None => {
            println!("Las matrices no son compatibles para la multiplicación.");
        }
    }
}

fn imprimir_matriz(matriz: &[Vec<i32>]) {
    for fila in matriz.iter() {
        println!("{:?}", fila);
    }
}
