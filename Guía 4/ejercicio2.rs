// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea una función que calcule el promedio de un arreglo de números enteros a partir del ingreso por parte del usuario de 10 números 
// de tipo entero. Reutiliza la función del ejercicio 1 para calcular la suma.

use std::io;

fn main() {
    let mut numeros = vec![];

    for i in 0..10 {
        println!("Ingrese el número {}:", i + 1);
        let mut entrada = String::new();
        io::stdin().read_line(&mut entrada).expect("Error al leer la entrada");
        let numero: i32 = entrada.trim().parse().expect("No se pudo convertir a número entero");

        numeros.push(numero);
    }

    let suma = suma_arreglo(&numeros);
    let promedio = promedio_arreglo(&numeros);

    println!("La suma de los números es {}", suma);
    println!("El promedio de los números es {}", promedio);
}

fn suma_arreglo(arr: &[i32]) -> i32 {
    let mut suma = 0;
    for &numero in arr {
        suma += numero;
    }
    suma
}

fn promedio_arreglo(arr: &[i32]) -> f64 {
    let suma = suma_arreglo(arr) as f64;
    let cantidad = arr.len() as f64;
    suma / cantidad
}
