// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Crea una función en rust que elimine los elementos duplicados de un arreglo, dejando solo una instancia de cada valor único.

fn eliminar_duplicados(arr: &mut Vec<i32>) {
    let mut i = 0;

    while i < arr.len() {
        let mut j = i + 1;

        while j < arr.len() {

            if arr[i] == arr[j] {
                arr.remove(j);
            } 
            else {
                j += 1;
            }
        }
        i += 1;
    }
}

fn main() {
    let mut numeros = vec![1, 1, 2, 2, 3, 4, 4, 5, 6, 6];
    println!("Arreglo original: {:?}", numeros);

    eliminar_duplicados(&mut numeros);
    println!("Arreglo sin duplicados: {:?}", numeros);
}