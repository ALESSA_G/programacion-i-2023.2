// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Escribe una función que genere un arreglo de los primeros "n" números de la secuencia de Fibonacci. Reutiliza la función de Fibonnaci 
// entregada en guias o ejemplos anteriores.

use std::io;

fn fibonacci(n: usize) -> Vec<u64> {
    if n == 0 {
        return Vec::new();
    }

    let mut serie_fib = vec![0, 1];

    while serie_fib.len() < n {
        let valor = serie_fib[serie_fib.len() - 1] + serie_fib[serie_fib.len() - 2];
        serie_fib.push(valor);
    }

    serie_fib
}

fn main() {
    println!("Ingrese el valor de 'n' para obtener los números de Fibonacci: ");

    let mut entrada = String::new();
    io::stdin().read_line(&mut entrada).expect("Error al leer la entrada");
    let n: usize = entrada.trim().parse().expect("No se pudo convertir a número");

    let resultado = fibonacci(n);

    println!("Los primeros {} números de Fibonacci son {:?}", n, resultado);
}