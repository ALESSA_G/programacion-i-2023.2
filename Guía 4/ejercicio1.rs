// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 02 de octubre del 2023

// Escribe una función que tome un arreglo de números enteros como entrada y devuelva la suma de todos los elementos del arreglo.


fn suma_arreglo(arr: &[i32]) -> i32 {
    let mut suma = 0;

    for &numero in arr {
        suma += numero;
    }
    suma
}

fn main() {
    let arreglo = [-1, 0, 1, 2, 3];
    let resultado = suma_arreglo(&arreglo);

    println!("La suma de los elementos del arreglo es {}", resultado);
}