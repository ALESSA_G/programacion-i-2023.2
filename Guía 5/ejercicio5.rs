// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Escribe una función que tome una matriz cuadrada como entrada y devuelva una nueva matriz que sea el resultado de rotar la matriz 90 grados en sentido horario.

fn rotar_matriz(matriz: Vec<Vec<i32>>) -> Vec<Vec<i32>> {

    let n = matriz.len();
    let mut matriz_rotada = vec![vec![0; n]; n];

    for i in 0..n {
        for j in 0..n {
            matriz_rotada[j][n - 1 - i] = matriz[i][j];
        }
    }

    matriz_rotada
}

fn main() {
    let matriz_inicial = vec![
        vec![1, 2, 3],
        vec![4, 5, 6],
        vec![7, 8, 9],
    ];

    let matriz_rotada = rotar_matriz(matriz_inicial.clone());

    println!("Matriz Original:");
    for fila in &matriz_inicial {
        println!("{:?}", fila);
    }

    println!("Matriz Rotada 90 grados en sentido horario:");
    for fila in &matriz_rotada{
        println!("{:?}", fila);
    }
}