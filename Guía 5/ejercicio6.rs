// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Crea una función que genere una matriz en forma de espiral. La matriz debe llenarse en espiral desde el centro hacia afuera.

fn matriz_espiral(n: usize) -> Vec<Vec<i32>> {
    
    let mut matriz = vec![vec![0; n]; n];

    let (mut izquierda, mut derecha, mut arriba, mut abajo) = (0, n - 1, 0, n - 1);

    let mut value = 1;


    while izquierda <= derecha && arriba <= abajo {
        
        for j in izquierda..=derecha {
            matriz[arriba][j] = value;
            value += 1;
        }
        arriba += 1;

        for i in arriba..=abajo {
            matriz[i][derecha] = value;
            value += 1;
        }
        derecha -= 1;

        if arriba <= abajo {
            for j in (izquierda..=derecha).rev() {
                matriz[abajo][j] = value;
                value += 1;
            }
            abajo -= 1;
        }

        if izquierda <= derecha {
            for i in (arriba..=abajo).rev() {
                matriz[i][izquierda] = value;
                value += 1;
            }
            izquierda += 1;
        }
    }
    matriz
}

fn main() {

    let n = 3;
    let spiral_matrix = matriz_espiral(n);

    for fila in &spiral_matrix {
        println!("{:?}", fila);
    }
}