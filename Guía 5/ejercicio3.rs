// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

//Implementa una función que tome una matriz como entrada y devuelva su matriz traspuesta. La matriz traspuesta se obtiene intercambiando filas por columnas.

fn matriz_traspuesta(matriz: Vec<Vec<i32>>) -> Vec<Vec<i32>> {
    let num_fila = matriz.len();
    let num_col = matriz[0].len();

    if num_fila == 0 {
        return vec![];
    }

    let mut matriz_traspuesta = vec![vec![0; num_fila]; num_col];

    for i in 0..num_fila {
        for j in 0..num_col {
            matriz_traspuesta[j][i] = matriz[i][j];
        }
    }

    matriz_traspuesta
}

fn main() {
    let input_matriz = vec![
        vec![1, 2, 3],
        vec![4, 5, 6],
        vec![7, 8, 9],
    ];

    let resultado = matriz_traspuesta(input_matriz.clone());

    println!("Matriz Original:");
    for fila in &input_matriz {
        println!("{:?}", fila);
    }

    println!("Matriz Traspuesta:");
    for fila in &resultado {
        println!("{:?}", fila);
    }
}