// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Escribe una función que tome una matriz y un número escalar como entrada y devuelva una nueva matriz que sea el resultado de multiplicar todos los elementos de la matriz por el escalar.

fn multiplicacion(matriz: &Vec<Vec<f64>>, escalar: f64) -> Vec<Vec<f64>> {
    matriz
        .iter()
        .map(|fila| fila.iter().map(|&x| x * escalar).collect())
        .collect()
}

fn main() {
    let matriz: Vec<Vec<f64>> = vec![
        vec![1.0, 2.0, 3.0],
        vec![4.0, 5.0, 6.0],
        vec![7.0, 8.0, 9.0],
    ];
    
    let escalar: f64 = 2.0;
    
    let resultado = multiplicacion(&matriz, escalar);
    
    println!("*Matriz inicial:");
    for fila in &matriz {
        for &element in fila {
            print!("{} ", element);
        }
        println!();
    }

    println!("*Escalar a utilizar: {}", escalar);
    
    println!("*Matriz final:");
    for fila in &resultado {
        for &element in fila {
            print!("{} ", element);
        }
        println!();
    }
}
