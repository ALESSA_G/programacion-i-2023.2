// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Implementa una función que verifique si una matriz es simétrica. Una matriz es simétrica si es igual a su matriz traspuesta.

fn matriz_simetrica(matriz: &Vec<Vec<i32>>) -> bool {

    let filas = matriz.len();
    if filas == 0 {
        return true; 
    }

    let col = matriz[0].len();

    if filas != col {
        return false;
    }

    for i in 0..filas {
        for j in 0..col {
            if matriz[i][j] != matriz[j][i] {
                return false; 
            }
        }
    }

    true 
}

fn main() {
    let matriz1: Vec<Vec<i32>> = vec![
        vec![1, 2, 3],
        vec![2, 4, 5],
        vec![3, 5, 6],
    ];

    let matriz2: Vec<Vec<i32> > = vec![
        vec![1, 2, 3],
        vec![4, 5, 6],
        vec![7, 8, 9],
    ];

    println!("Matriz 1:");
    for fila in &matriz1 {
        println!("{:?}", fila);
    }

    if matriz_simetrica(&matriz1) {
        println!("La matriz es simétrica.");
    } 
    else {
        println!("La matriz no es simétrica.");
    }

    println!("Matriz 2:");
    for fila in &matriz2 {
        println!("{:?}", fila);
    }

    if matriz_simetrica(&matriz2) {
        println!("La matriz es simétrica.");
    } 
    else {
        println!("La matriz no es simétrica.");
    }
}
