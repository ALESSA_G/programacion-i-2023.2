// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Escribe una función que genere una matriz n x n con un diamante en su interior, tal cual la siguiente salida.
//        ∗
//      ∗ ∗ ∗
//     ∗ ∗ ∗ ∗ ∗
//   ∗ ∗ ∗ ∗ ∗ ∗ ∗
// ∗ ∗ ∗ ∗ ∗ ∗ ∗ ∗ ∗
//   ∗ ∗ ∗ ∗ ∗ ∗ ∗
//     ∗ ∗ ∗ ∗ ∗
//       ∗ ∗ ∗
//         ∗

fn diamante_matriz(n: usize) -> Vec<Vec<char>> {
    let mut matriz = vec![vec![' '; n]; n];

    let diamante_central = n / 2;

    for i in 0..diamante_central + 1 {
        for j in diamante_central - i..diamante_central + i + 1 {
            matriz[i][j] = '*';
            matriz[n - i - 1][j] = '*';
        }
    }

    matriz
}

fn main() {

    let n = 7;
    let diseño_final = diamante_matriz(n);

    for fila in &diseño_final {
        for &caracter in fila {
            print!(" {}", caracter);
        }
        println!();
    }
}
