// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 18 de octubre del 2023

// Crea una función que genere y devuelva una matriz identidad de tamaño n x n. Una matriz identidad es una matriz cuadrada en la que todos los elementos en la diagonal principal son 1 y los demás son 0.

fn matriz_identidad(n: usize) -> Vec<Vec<i32>> {
    let mut matriz = vec![vec![0; n]; n];

    for i in 0..n {
        matriz[i][i] = 1;
    }

    matriz
}

fn main() {
    let n = 5; // Cambia este valor según el tamaño de matriz que desees
    let identidad = matriz_identidad(n);

    for fila in &identidad {
        for &elemento in fila {
            print!("{} ", elemento);
        }
        println!();
    }
}