// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Escriba un programa que pregunte al usuario la hora actual t del reloj y un número entero de horas h, 
// que indique qué hora marcará el reloj dentro de h horas.

use std::io;

fn main() {
    
    println!("Por favor, ingrese la hora actual: ");

    let mut hora_actual = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut hora_actual).unwrap();

    let hora_actual: i32 = hora_actual.trim().parse().expect("Entrada no válida. Ingrese un número entero.");

    println!("Ingrese el número de horas a avanzar: ");

    let mut horas_avanzar = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut horas_avanzar).unwrap();

    let horas_avanzar: i32 = horas_avanzar.trim().parse().expect("Entrada no válida. Ingrese un número entero.");

    let hora_futura = calcular_hora(hora_actual, horas_avanzar);
    
    println!("La hora que marcará el reloj dentro de {} horas será: {}", horas_avanzar, hora_futura);
}

fn calcular_hora(hora_actual: i32, horas_avanzar: i32) -> i32 {

    let horas_por_dia = 24;
    let hora_futura = (hora_actual + horas_avanzar) % horas_por_dia;

    if hora_futura < 0 {
        hora_futura + horas_por_dia
    } 
    else {
        hora_futura
    }
}