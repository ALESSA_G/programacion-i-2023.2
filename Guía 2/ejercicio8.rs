// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Escriba una función que tome una cadena de texto como entrada y devuleva la última letra de la cadena.

fn ultima_letra(cadena: &str) -> char {

    cadena.chars().rev().next().unwrap_or('\0')
}

fn main() {
    let cadena = "Hola mundo";
    let letra = ultima_letra(cadena);

    if letra != '\0' {
        println!("Cadena: {}", cadena);
        println!("La última letra de la cadena es: {}", letra);
    } else {
        println!("La cadena está vacía.");
    }
}