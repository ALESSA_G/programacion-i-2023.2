// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Escribir un programa que indique si un año es bisiesto o no, teniendo en cuenta cuál era el calendario 
// vigente en ese año.

use std::io;

fn main() {

    println!("Ingrese un año para verificar si es bisiesto:");

    let mut entrada = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut entrada).unwrap();

    let año: i32 = entrada.trim().parse().expect("Entrada no válida. Ingrese un número entero.");

    if bisiesto(año) {
        println!("El año {} es bisiesto.", año);

    } else {
        println!("El año {} no es bisiesto.", año);
    }
}

fn bisiesto(año: i32) -> bool {
    // Un año es bisiesto si es divisible por 4, excepto si es divisible por 100 pero divisible por 400.
    (año % 4 == 0 && año % 100 != 0) || (año % 400 == 0)
}