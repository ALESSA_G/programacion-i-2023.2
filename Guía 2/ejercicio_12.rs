// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha 04 de septiembre del 2023.

// Escribe una función en Rust llamada "convertir a fahrenheit" que tome un parámetro de tipo f32 que
// represente una temperatura en Celsius y devuelva la temperatura equivalente en Fahrenheit. Luego,
// utiliza esta función para convertir una temperatura ingresada por el usuario a Fahrenheit.

use std::io;

fn convertir_a_fahrenheit(temperatura_celsius: f32) -> f32 {
    // F = (C × 9/5) + 32
    (temperatura_celsius * 9.0 / 5.0) + 32.0
}

fn main() {
    
    println!("Ingrese la temperatura en grados Celsius:");

    let mut entrada = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut entrada).unwrap();

    let temperatura_celsius: f32 = entrada.trim().parse().expect("Entrada no válida. Ingrese un número válido.");

    let temperatura_fahrenheit = convertir_a_fahrenheit(temperatura_celsius);

    println!(
        "{} grados Celsius son equivalentes a {} grados Fahrenheit.", temperatura_celsius, temperatura_fahrenheit);
}