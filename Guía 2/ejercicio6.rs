// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha 04 de septiembre del 2023

// Escriba una función que tome dos números como entrada y devuelva el número mayor.

fn num_mayor(valor1: i32, valor2: i32) -> i32 {

    if valor1 > valor2 {
        valor1
    } 
    else {
        valor2
    }
}

fn main() {
    let valor1 = 42;
    let valor2 = 27;

    let resultado = num_mayor(valor1, valor2);

    println!("El número mayor entre {} y {} es: {}", valor1, valor2, resultado);
}