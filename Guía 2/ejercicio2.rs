// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Cree una función que tome dos parámetros: una cadena como parámetro y una letra. La función debe devolver 
// el número de veces que aparece el parámetro "letra" en la cadena.

fn contador_letra_en_cadena(cadena: &str, letra: char) -> usize {

    let mut contador = 0;

    for caracter in cadena.chars() {

        if caracter == letra {
            contador += 1;
        }
    }

    contador
}

fn main() {

    let cadena = "Hola, buenos dias.";
    let letra = 'a';

    let resultado = contador_letra_en_cadena(cadena, letra);

    println!("Cadena: {}", cadena);
    println!("La letra '{}' aparece {} veces en la cadena.", letra, resultado);
    
}