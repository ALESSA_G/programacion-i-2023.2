// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Cree un procedimiento que tome una cadena como entrada y devuelva una nueva cadena con todas las vocales 
// eliminadas. Este procedimiento se debe implementar utilizando un bucle loop. El bucle itera sobre los 
// carácteres de la cadena y elimina las vocales de la cadena.

use std::io;

fn eliminar_vocales(cadena: &str) -> String {

    let mut resultado = String::new();

    for caracter in cadena.chars() {
        match caracter {
            'a' | 'e' | 'i' | 'o' | 'u' | 'A' | 'E' | 'I' | 'O' | 'U' => continue,
            _ => resultado.push(caracter),
        }
    }
    resultado
}

fn main() {

    println!("Ingrese una cadena:");

    let mut entrada = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut entrada).unwrap();

    let resultado = eliminar_vocales(&entrada);

    println!("Cadena: {}", entrada);
    println!("La cadena sin vocales es: {}", resultado);
}