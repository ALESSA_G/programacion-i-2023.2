// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha 04 de septiembre del 2023

// Escriba un procedimiento que tome una cadena como entrada y devuelva el número de vocales en la cadena.

use std::io;

fn contar_vocales(cadena: &str) -> usize {

    let mut contador = 0;

    for c in cadena.chars() {
        match c {
            'a' | 'A' | 'e' | 'E' | 'i' | 'I' | 'o' | 'O' | 'u' | 'U' => contador += 1,
            _ => (),
        }
    }
    contador
}

fn main() {
    
    println!("Por favor, ingresa una cadena de texto:");
    
    let mut input = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut input).unwrap();

    let num_vocales = contar_vocales(&input);

    println!("Número de vocales presentes en la cadena: {}", num_vocales);
}