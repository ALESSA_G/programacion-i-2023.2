// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Realice la conjetura de ULAM (Si es un número par, divida por 2, si es impar divida por tres y sume 1.)

use std::io;

fn main() {
    println!("*Conjetura de ULAM*");
    println!("Ingrese un número entero positivo para comenzar:");

    let mut entrada = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut entrada).unwrap();
    
    let mut numero = entrada.trim().parse::<u32>().expect("Entrada no válida");

    while numero != 1 {

        println!("{}", numero);

        if numero % 2 == 0 {
            numero /= 2;
        }

        else {
            numero = 3 * numero + 1;
        }
    }

    println!("1");
    println!("Conjetura de Ulam completada.");

}