// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Escriba una función que tome dos palabras, compare letra a letra la primera palabra contra la segunda, y 
// devuelva las letras que no estén contenidas en la segunda palabra.

fn letras_no_contenidas(palabra1: &str, palabra2: &str) -> String {

    let mut letras_no_contenidas = String::new();

    for letra1 in palabra1.chars() {
        
        if !palabra2.contains(letra1) {
            letras_no_contenidas.push(letra1);
        }
    }

    letras_no_contenidas
}

fn main() {
    
    let palabra1 = "hola";
    let palabra2 = "ciao";

    let resultado = letras_no_contenidas(palabra1, palabra2);

    println!("Letras no contenidas en la segunda palabra: {}", resultado);
}