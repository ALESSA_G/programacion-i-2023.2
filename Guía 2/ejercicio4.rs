// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 04 de septiembre del 2023

// Escriba un programa que, dado N cantidad de números ingresados por el usuario, vaya mostrando siempre el 
// mayor y menor de esta N cantidad. La comparación debe ser en funciones.

use std::io;

fn main() {

    println!("Ingrese la cantidad de números: ");
    let mut cant_n = String::new();

    let stdin = io::stdin();
    stdin.read_line(&mut cant_n).unwrap();

    let n: usize = cant_n.trim().parse().expect("Por favor, ingrese un número válido.");

    let (mut menor, mut mayor) = (i32::MAX, i32::MIN);

    for i in 0..n {

        println!("Ingrese el número {}: ", i + 1);
        let mut numero = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut numero).unwrap();

        let numero: i32 = numero.trim().parse().expect("Por favor, ingrese un número válido.");

        if numero < menor {
            menor = numero;
        }

        if numero > mayor {
            mayor = numero;
        }

        println!("Número menor hasta ahora: {}", menor);
        println!("Número mayor hasta ahora: {}", mayor);
    }
}