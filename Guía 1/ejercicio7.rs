// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023 

// Escriba un programa que defina una variable, un número, y solicite al usuario que ingrese un número.
// Luego, el programa debería imprimir el factorial del número.

use std::io;

fn main() {
    let mut numero = String::new();
    let stdin = io::stdin();

    println!("Por favor, ingrese un número: ");

    stdin.read_line(&mut numero).unwrap();
    let numero:u32 = numero.trim().parse().unwrap();

    let factorial_resultado = factorial(numero);
    println!("El factorial de {} es: {}", numero, factorial_resultado);

}

fn factorial(numero: u32) -> u32 {
    
    let mut resultado = 1;
    let mut i: u32 = 1;

    while i <= numero {
        resultado *= i;
        i += 1;
    }

    resultado
}
