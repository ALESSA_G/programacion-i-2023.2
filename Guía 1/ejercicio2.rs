// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023 

// Escriba un programa que defina una variable, un nombre, y le pida al usuario que ingrese su nombre. 
// Luego, el programa debería imprimir el nombre que ingresó el usuario.

use std::io;

fn main () {

    let mut nombre = String::new();
    let stdin = io :: stdin();
    println!( "Por favor, ingrese su nombre: ");
    stdin.read_line (& mut nombre).unwrap();
    
    println!( "Excelente, su nombre es {}", nombre) ;

}
