// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023

// Escriba un programa que defina una variable, un promedio, y la inicialice a 0. Luego, el programa debería 
// pedirle al usuario que ingrese una serie de números. Luego, el programa debe sumar cada número al promedio 
// variable y dividir la suma por el número de números ingresados. Luego, el programa debería imprimir el 
// promedio de los números.

use std::io;

fn main() {
    let mut suma = 0.0;
    let mut contador = 0;

    println!("Ingrese una serie de números (Enter para terminar): ");

    loop {
        let mut input = String::new();
        let stdin = io::stdin();
        stdin.read_line(&mut input).unwrap();

        match input.trim().parse::<f64>() {
            Ok(numero) => {
                suma += numero;
                contador += 1;
            }
            Err(_) => break,
        }
    }

    if contador > 0 {
        let average = suma / contador as f64;
        println!("Promedio final: {}", average);
    } else {
        println!("No se ingresaron números para calcular el promedio.");
    }
}