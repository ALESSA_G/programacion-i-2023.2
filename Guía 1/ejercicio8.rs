// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023

// Escriba un programa que defina una variable, una suma, y la inicialice en 0. Luego, el programa debería
// pedirle al usuario que ingrese una serie de números. Luego, el programa debe sumar cada número a la suma 
// variable e imprimir la suma final.

use std::io;

fn main() {

    let mut suma = 0;

    println!("Ingrese una serie de números (Presione Enter para finalizar): ");

    loop {
        let mut entrada = String::new();
        let stdin = io :: stdin();
        stdin.read_line (& mut entrada).unwrap();

        if entrada.trim().is_empty() {
            break;
        }

        let numero: i32 = match entrada.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Entrada no válida. Por favor, ingrese un número válido.");
                continue;
            }
        };

        suma += numero;
    }
    
    println!("La suma total de la serie es: {}", suma);
}
