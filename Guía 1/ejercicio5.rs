// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023 

// Escriba un programa que defina una variable, un contador, y lo inicialice a 0. 
// Luego, el programa debería imprimir los números del 0 al 9, uno por línea.

fn main() {
    
    let mut contador = 0;

    println!("Contador del 0 al 9:");

    loop {
        println!("{}", contador);
        contador += 1;

        if contador == 10 {
            break;
        }
    }
}
