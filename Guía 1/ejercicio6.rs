// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023 

// Escriba un programa que defina una variable, un contador, y lo inicialice en 10. Luego, el programa
// debería imprimir los números del 10 al 0, uno por línea.

fn main() {

    let mut contador = 10;

    println!("Contador del 10 al 0:");

    loop {
        println!("{}", contador );
        contador += -1;

        if contador == -1 {
            break;
        }
    }
}
