// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023

//Escriba un programa que defina una variable, un número, y solicite al usuario que ingrese un número.
//Luego, el programa debería imprimir el mensaje “El núumero es par” si el núumero es par, y el mensaje
//“El número es impar” si el núumero es impar.

use std::io;

fn main() {
    let mut num = String::new();
    let stdin = io::stdin();

    println!("Por favor, ingrese un número: ");

    stdin.read_line(&mut num).unwrap();
    let num:i8 = num.trim().parse().unwrap();

    if num % 2 == 0 {
        println!("El número {} es par.", num);
    }
    else {
        println!("El número {} es impar.", num);
    }

}
