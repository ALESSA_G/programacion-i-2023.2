// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023

// Escriba un programa que defina una variable, producto, y lo inicialice en 1. Luego, el programa debería
// pedirle al usuario que ingrese una serie de números. Luego, el programa debería multiplicar cada número
// por el producto variable e imprimir el producto final.

use std::io;

fn main() {
    let mut producto = 1.0;

    println!("Ingrese una serie de números separados por espacios: ");

    let mut input = String::new();
    let stdin = io :: stdin();
    stdin.read_line (& mut input).unwrap();

    for numero_str in input.split_whitespace() {
        
        if let Ok(numero) = numero_str.parse::<f64>() {
            producto *= numero;
        }
    }

    println!("El producto de los números ingresados es: {}", producto);
}
