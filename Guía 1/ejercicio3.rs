// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023 

// Escriba un programa que defina una variable, la edad, y solicite al usuario que ingrese su edad. 
// Luego, el programa debería imprimir el mensaje “Tienes edad suficiente para votar” si el usuario tiene 18 años o más, 
// y el mensaje “No tienes edad suficiente para votar” si el usuario es menor de 18 años.

use std::io;

fn main() {
    let mut edad = String::new();
    let stdin = io::stdin();

    println!("Por favor, ingrese su edad: ");

    stdin.read_line(&mut edad).unwrap();
    let edad:i8 = edad.trim().parse().unwrap();

    if edad >= 18 {
        println!("Excelente, tiene edad suficiente para votar.");
    }
    else if edad < 0 {
        println!("Por favor, ingrese un número válido.");
    }
    else {
        println!("Lo sentimos, no tiene edad suficiente para votar.");
    }
}
