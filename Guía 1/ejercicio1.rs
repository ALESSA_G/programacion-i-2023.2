// Nombre: Gabriela Antonia Sepúlveda Rojas
// RUT: 20.787.581-3
// Fecha: 28 de agosto del 2023

// Escriba un programa que defina dos variables, x e y, e imprima su suma.

fn main() {
    imprimir_sum(-8, 6);
}

fn imprimir_sum(x: i32, y: i32) {

    println!("La suma de {} y {} es: {}", x, y, x + y);

}
